package starter.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class OrangeHRMHomePage extends PageObject {

    public void enterUsername(){
        $(By.id("txtUsername")).type("Admin");
    }

    public void enterPassword(){
        $(By.id("txtPassword")).type("admin123");
    }

    public void  btnLogin(){
        $(By.id("btnLogin")).click();
    }
}
