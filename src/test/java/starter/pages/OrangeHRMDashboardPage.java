package starter.pages;

import net.serenitybdd.core.pages.PageObject;

public class OrangeHRMDashboardPage extends PageObject {

    public void loginVerified(){
        String actualTitle = getDriver().getTitle();
        String expectedTitle = "OrangeHRM";
        if (actualTitle.equals(expectedTitle)){
            System.out.println("Title is "+actualTitle);
        }
    }
}
