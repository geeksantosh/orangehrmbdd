package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import starter.pages.Dashboard;
import starter.pages.HomePage;

public class LoginToApplication {

    @Steps
    HomePage homePage;
    @Steps
    Dashboard dashboard;

    @Given("User is on home page")
    public void user_is_on_home_page() {
    homePage.openApplication();
    }

    @When("User enter Admin as username")
    public void user_enter_Admin_as_username() {
    homePage.enterUsername();
    }

    @When("User enter admin123 as password")
    public void user_enter_admin123_as_password() {
    homePage.enterPassword();
    homePage.clickLoginBtn();
    }

    @Then("User should be able to login")
    public void user_should_be_able_to_login() {
    dashboard.verifyAdminLogin();
    }
}
